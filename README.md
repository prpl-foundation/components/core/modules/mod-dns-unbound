mod-dns-unbound
==============

description
--------
The process that loads this module will generate Unbound's configuration file `/etc/unbound/unbound.conf`. It will start Unbound as a child process and wait (async) for Unbound to register itself on the (u)bus with datamodel `Unbound.`. From here on end the amx bus api is used except for features not supported by Unbound's `unbound-control` tool, for those exceptions the child is killed and a new Unbound process is spawned.

Requires:
- Unbound version 1.17 (for interface style acl).
- Unbound must be patched for the amx bus api.

features
--------

| feature | supported | requires restart of unbound daemon |
| :---    | :---:     | :---:                              |
| add server | :white_check_mark: | :x: |
| remove server | :white_check_mark: | :x: |
| add host | :white_check_mark: | :x: |
| remove host | :white_check_mark: | :x: |
| add interface | :white_check_mark: | :white_check_mark: |
| remove interface | :white_check_mark: | :white_check_mark: |
| report server failure | :white_check_mark: | |
| set cache | :white_check_mark: | :white_check_mark: |
| set options | :white_check_mark: | :x: |
| flush cache | :white_check_mark: | :x: |

debugging
-----
For debugging or prototyping, Unbound options given with the `set-options` function are applied to Unbound's `.conf` file. E.g.:
```
# ODL of tr181-dns plugin
%config {
    modules = {
        dns-options = [
            "log-queries: yes",
            "log-replies: yes",
            "verbosity: 1"
        ],
    };
}
```

compile + (unit)test + install
------------------------

```
# compile
make

# test
make test

# install
sudo make install
```