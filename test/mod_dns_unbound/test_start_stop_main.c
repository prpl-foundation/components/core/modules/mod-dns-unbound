/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "test_start_stop.h"

int main(void) {
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_add_server),
        cmocka_unit_test(test_add_interface),
        cmocka_unit_test(test_set_domain_name_without_host),
        cmocka_unit_test(test_add_server_while_unbound_is_running),
        cmocka_unit_test(test_add_local_host_while_unbound_is_running),
        cmocka_unit_test(test_set_domain_name),
        cmocka_unit_test(test_add_external_host_while_unbound_is_running_without_view),
        cmocka_unit_test(test_add_external_host_while_unbound_is_running),
        cmocka_unit_test(test_set_cache),
        cmocka_unit_test(test_flush_cache_while_unbound_is_running),
        cmocka_unit_test(test_server_failure),
        cmocka_unit_test(test_enable_changed),
        cmocka_unit_test(test_rebind_changed),
        cmocka_unit_test(test_remove_local_host_address_while_unbound_is_running),
        cmocka_unit_test(test_remove_local_host_while_unbound_is_running),
        cmocka_unit_test(test_remove_external_host_while_unbound_is_running),
        cmocka_unit_test(test_remove_interface),
        cmocka_unit_test(test_add_local_host_while_unbound_is_running_and_interface_deleted),
        cmocka_unit_test(test_add_interface),
        cmocka_unit_test(test_remove_server_while_unbound_is_running),
        cmocka_unit_test(test_add_fwzone),
        cmocka_unit_test(test_remove_fwzone),
    };
    return cmocka_run_group_tests(tests, test_setup, test_teardown);
}
