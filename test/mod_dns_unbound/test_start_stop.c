/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <string.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>
#include <amxm/amxm.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_timer.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "test_start_stop.h"
#include "mock.h"
#include "mock_dm.h"

#define handle_events amxut_bus_handle_events

static amxd_dm_t* dm = NULL;
static amxo_parser_t* parser = NULL;

int test_setup(void** state) {
    amxm_shared_object_t* so = NULL;
    amxm_module_t* mod = NULL;

    amxut_bus_setup(state);
    dm = amxut_bus_dm();
    parser = amxut_bus_parser();
    mock_dm_init(dm, parser, amxd_dm_get_root(dm));

    amxm_module_register(&mod, NULL, "core");
    amxm_module_add_function(mod, "server-failure", ut_server_failure);

    amxp_sigmngr_add_signal(NULL, "wait:done"); // otherwise modunbound cannot do amxp_slot_connect(..., "wait:done", ...)

    assert_int_equal(amxm_so_open(&so, "mod", MOD_DIR "/mod-dns-unbound.so"), 0);
    handle_events();

    return 0;
}

int test_teardown(void** state) {
    amxm_close_all();
    amxut_bus_teardown(state);
    return 0;
}

void test_add_server(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t uc_ret;
    int rv = -1;
    uint32_t count = ut_get_number_of_subproc_restarts();

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_init(&uc_ret);
    amxc_var_set(cstring_t, &uc_ret, "");

    print_message("add server srv-1\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", "srv-1");
    amxc_var_add_key(cstring_t, &args, "server", "8.8.8.8");
    amxc_var_add_key(bool, &args, "enable", true);
    rv = amxm_execute_function("mod", "dns", "add-server", &args, &ret);
    assert_int_equal(rv, 0);
    amxut_timer_go_to_future_ms(500); // start delay
    handle_events();
    assert_int_equal(count + 1, ut_get_number_of_subproc_restarts());

    count = ut_get_number_of_subproc_restarts();
    print_message("add interface eth0\n");
    print_message("expectation: subproc should not be restarted\n");
    expect_check(_UnboundControl, args, args_equal_check, "interface eth0,6000");
    will_return(_UnboundControl, &uc_ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "interface", "eth0");
    rv = amxm_execute_function("mod", "dns", "set-interface", &args, &ret);
    assert_int_equal(rv, 0);
    handle_events();
    assert_int_equal(count, ut_get_number_of_subproc_restarts());

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&uc_ret);
}

void test_add_interface(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t uc_ret;
    int rv = -1;
    uint32_t count = ut_get_number_of_subproc_restarts();

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_init(&uc_ret);
    amxc_var_set(cstring_t, &uc_ret, "");

    print_message("add interface br-lan\n");
    print_message("expectation: subproc should not be restarted\n");
    expect_check(_UnboundControl, args, args_equal_check, "interface br-lan,6000");
    will_return(_UnboundControl, &uc_ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "interface", "br-lan");
    rv = amxm_execute_function("mod", "dns", "set-interface", &args, &ret);
    assert_int_equal(rv, 0);
    assert_int_equal(count, ut_get_number_of_subproc_restarts());
    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&uc_ret);
}

void test_set_domain_name_without_host(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;
    uint32_t count = ut_get_number_of_subproc_restarts();

    amxc_var_init(&args);
    amxc_var_init(&ret);

    print_message("add domain name lan\n");
    print_message("expectation: subproc should not be restarted\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "interface", "br-lan");
    amxc_var_add_key(cstring_t, &args, "domain_name", "lan");
    rv = amxm_execute_function("mod", "dns", "set-domain-name", &args, &ret);
    assert_int_equal(rv, 0);
    assert_int_equal(count, ut_get_number_of_subproc_restarts());
    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_set_domain_name(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t uc_ret1;
    amxc_var_t uc_ret2;
    amxc_var_t uc_ret3;
    int rv = -1;
    uint32_t count = ut_get_number_of_subproc_restarts();

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_init(&uc_ret1);
    amxc_var_set(cstring_t, &uc_ret1, "ok");

    amxc_var_init(&uc_ret2);
    amxc_var_set(cstring_t, &uc_ret2, "Adr-s-A55. 3600    IN      A       192.168.30.11\nprplOS.home.arpa.	3600	IN	A	192.168.30.1\n");

    amxc_var_init(&uc_ret3);
    amxc_var_set(cstring_t, &uc_ret3, "added 4 datas");

    print_message("add domain name lan\n");
    print_message("expectation: subproc should not be restarted\n");
    expect_check(_UnboundControl, args, args_equal_check, "view_local_data_remove br-lan mytvbox");
    will_return(_UnboundControl, &uc_ret1);
    expect_check(_UnboundControl, args, args_equal_check, "view_local_data_remove br-lan mytvbox.lan");
    will_return(_UnboundControl, &uc_ret1);
    expect_check(_UnboundControl, args, args_equal_check, "view_list_local_data br-lan");
    will_return(_UnboundControl, &uc_ret2);
    expect_check(_UnboundControl, args, args_equal_check,
                 "view_local_datas2 br-lan,"
                 "mytvbox IN A 192.168.1.11,"
                 "mytvbox.home IN A 192.168.1.11,"
                 "mytvbox IN A 192.168.1.12,"
                 "mytvbox.home IN A 192.168.1.12,"
                 "mytvbox IN AAAA fe80::11,"
                 "mytvbox.home IN AAAA fe80::11,"
                 "mytvbox IN AAAA fe80::12,"
                 "mytvbox.home IN AAAA fe80::12");
    will_return(_UnboundControl, &uc_ret3);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "interface", "br-lan");
    amxc_var_add_key(cstring_t, &args, "domain_name", "home");
    rv = amxm_execute_function("mod", "dns", "set-domain-name", &args, &ret);
    assert_int_equal(rv, 0);
    assert_int_equal(count, ut_get_number_of_subproc_restarts());
    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&uc_ret1);
    amxc_var_clean(&uc_ret2);
    amxc_var_clean(&uc_ret3);
}

void test_add_server_while_unbound_is_running(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;
    uint32_t count = ut_get_number_of_subproc_restarts();
    amxc_var_t uc_ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_init(&uc_ret);
    amxc_var_set(cstring_t, &uc_ret, "ok");

    print_message("add server srv-2\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", "srv-2");
    amxc_var_add_key(cstring_t, &args, "server", "8.8.4.4");
    amxc_var_add_key(bool, &args, "enable", true);
    print_message("expectation: subproc should not be restarted for adding a second server\n");
    expect_check(_UnboundControl, args, args_equal_check, "forward_add . 8.8.4.4 8.8.8.8");
    will_return(_UnboundControl, &uc_ret);
    rv = amxm_execute_function("mod", "dns", "add-server", &args, &ret);
    assert_int_equal(rv, 0);
    assert_int_equal(count, ut_get_number_of_subproc_restarts());
    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&uc_ret);
}

void test_add_local_host_while_unbound_is_running(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* var = NULL;
    int rv = -1;
    uint32_t count = ut_get_number_of_subproc_restarts();
    amxc_var_t uc_ret;
    amxc_var_t uc_ret2;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_init(&uc_ret);
    amxc_var_set(cstring_t, &uc_ret, "added 4 datas");

    amxc_var_init(&uc_ret2);
    amxc_var_set(cstring_t, &uc_ret2, "Adr-s-A55. 3600    IN      A       192.168.30.11\nprplOS.home.arpa.	3600	IN	A	192.168.30.1\n");

    print_message("add host mytvbox\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", "mytvbox");
    amxc_var_add_key(cstring_t, &args, "interface", "br-lan");
    var = amxc_var_add_key(amxc_llist_t, &args, "ipv4_addresses", NULL);
    amxc_var_add(cstring_t, var, "192.168.1.11");
    amxc_var_add(cstring_t, var, "192.168.1.12");
    var = amxc_var_add_key(amxc_llist_t, &args, "ipv6_addresses", NULL);
    amxc_var_add(cstring_t, var, "fe80::11");
    amxc_var_add(cstring_t, var, "fe80::12");

    print_message("expectation: subproc should not be restarted\n");
    expect_check(_UnboundControl, args, args_equal_check, "view_list_local_data br-lan");
    will_return(_UnboundControl, &uc_ret2);
    expect_check(_UnboundControl, args, args_equal_check,
                 "view_local_datas2 br-lan,"
                 "mytvbox IN A 192.168.1.11,"
                 "mytvbox.lan IN A 192.168.1.11,"
                 "mytvbox IN A 192.168.1.12,"
                 "mytvbox.lan IN A 192.168.1.12,"
                 "mytvbox IN AAAA fe80::11,"
                 "mytvbox.lan IN AAAA fe80::11,"
                 "mytvbox IN AAAA fe80::12,"
                 "mytvbox.lan IN AAAA fe80::12");
    will_return(_UnboundControl, &uc_ret);
    rv = amxm_execute_function("mod", "dns", "add-host", &args, &ret);
    assert_int_equal(rv, 0);
    assert_int_equal(count, ut_get_number_of_subproc_restarts());
    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&uc_ret);
    amxc_var_clean(&uc_ret2);
}

void test_add_local_host_while_unbound_is_running_and_interface_deleted(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* var = NULL;
    int rv = -1;
    uint32_t count = ut_get_number_of_subproc_restarts();
    amxc_var_t uc_ret;
    amxc_var_t uc_ret2;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_init(&uc_ret);
    amxc_var_set(cstring_t, &uc_ret, "added 4 datas");

    amxc_var_init(&uc_ret2);
    amxc_var_set(cstring_t, &uc_ret2, "Adr-s-A55. 3600    IN      A       192.168.30.11\nprplOS.home.arpa.	3600	IN	A	192.168.30.1\n");

    print_message("add host mytvbox\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", "mytvbox");
    amxc_var_add_key(cstring_t, &args, "interface", "br-lan");
    var = amxc_var_add_key(amxc_llist_t, &args, "ipv4_addresses", NULL);
    amxc_var_add(cstring_t, var, "192.168.1.11");
    amxc_var_add(cstring_t, var, "192.168.1.12");
    var = amxc_var_add_key(amxc_llist_t, &args, "ipv6_addresses", NULL);
    amxc_var_add(cstring_t, var, "fe80::11");
    amxc_var_add(cstring_t, var, "fe80::12");

    print_message("expectation: subproc should not be restarted\n");
    expect_check(_UnboundControl, args, args_equal_check, "view_list_local_data br-lan");
    will_return(_UnboundControl, &uc_ret2);
    expect_check(_UnboundControl, args, args_equal_check,
                 "view_local_datas2 br-lan,"
                 "mytvbox IN A 192.168.1.11,"
                 "mytvbox IN A 192.168.1.12,"
                 "mytvbox IN AAAA fe80::11,"
                 "mytvbox IN AAAA fe80::12");
    will_return(_UnboundControl, &uc_ret);
    rv = amxm_execute_function("mod", "dns", "add-host", &args, &ret);
    assert_int_equal(rv, 0);
    assert_int_equal(count, ut_get_number_of_subproc_restarts());
    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&uc_ret);
    amxc_var_clean(&uc_ret2);
}

void test_add_external_host_while_unbound_is_running_without_view(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* var = NULL;
    int rv = -1;
    uint32_t count = ut_get_number_of_subproc_restarts();
    amxc_var_t uc_ret;
    amxc_var_t uc_ret2;
    amxc_var_t uc_ret3;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_init(&uc_ret);
    amxc_var_set(cstring_t, &uc_ret, "added 4 datas");

    amxc_var_init(&uc_ret2);
    amxc_var_set(cstring_t, &uc_ret2, "no view with name: br-lan\n");

    amxc_var_init(&uc_ret3);
    amxc_var_set(cstring_t, &uc_ret3, "view added: br-lan\n");

    print_message("add host example.com\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", "withoutview.com");
    amxc_var_add_key(cstring_t, &args, "interface", "br-lan");
    var = amxc_var_add_key(amxc_llist_t, &args, "ipv4_addresses", NULL);
    amxc_var_add(cstring_t, var, "123.456.789.10");
    var = amxc_var_add_key(amxc_llist_t, &args, "ipv6_addresses", NULL);
    amxc_var_add(cstring_t, var, "fe80::10");

    print_message("expectation: subproc should not be restarted\n");
    expect_check(_UnboundControl, args, args_equal_check, "view_list_local_data br-lan");
    will_return(_UnboundControl, &uc_ret2);
    expect_check(_UnboundControl, args, args_equal_check, "view br-lan,yes");
    will_return(_UnboundControl, &uc_ret3);
    expect_check(_UnboundControl, args, args_equal_check,
                 "view_local_datas2 br-lan,"
                 "withoutview.com IN A 123.456.789.10,"
                 "withoutview.com IN AAAA fe80::10");
    will_return(_UnboundControl, &uc_ret);
    rv = amxm_execute_function("mod", "dns", "add-host", &args, &ret);
    assert_int_equal(rv, 0);
    assert_int_equal(count, ut_get_number_of_subproc_restarts());
    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&uc_ret);
    amxc_var_clean(&uc_ret2);
    amxc_var_clean(&uc_ret3);
}

void test_add_external_host_while_unbound_is_running(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* var = NULL;
    int rv = -1;
    uint32_t count = ut_get_number_of_subproc_restarts();
    amxc_var_t uc_ret;
    amxc_var_t uc_ret2;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_init(&uc_ret);
    amxc_var_set(cstring_t, &uc_ret, "added 4 datas");

    amxc_var_init(&uc_ret2);
    amxc_var_set(cstring_t, &uc_ret2, "Adr-s-A55. 3600    IN      A       192.168.30.11\nprplOS.home.arpa.	3600	IN	A	192.168.30.1\n");

    print_message("add host example.com\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", "example.com");
    amxc_var_add_key(cstring_t, &args, "interface", "br-lan");
    var = amxc_var_add_key(amxc_llist_t, &args, "ipv4_addresses", NULL);
    amxc_var_add(cstring_t, var, "123.456.789.10");
    var = amxc_var_add_key(amxc_llist_t, &args, "ipv6_addresses", NULL);
    amxc_var_add(cstring_t, var, "fe80::10");

    print_message("expectation: subproc should not be restarted\n");
    expect_check(_UnboundControl, args, args_equal_check, "view_list_local_data br-lan");
    will_return(_UnboundControl, &uc_ret2);
    expect_check(_UnboundControl, args, args_equal_check,
                 "view_local_datas2 br-lan,"
                 "example.com IN A 123.456.789.10,"
                 "example.com IN AAAA fe80::10");
    will_return(_UnboundControl, &uc_ret);
    rv = amxm_execute_function("mod", "dns", "add-host", &args, &ret);
    assert_int_equal(rv, 0);
    assert_int_equal(count, ut_get_number_of_subproc_restarts());
    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&uc_ret);
    amxc_var_clean(&uc_ret2);
}

void test_set_cache(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* var = NULL;
    int rv = -1;
    uint32_t count = ut_get_number_of_subproc_restarts();

    amxc_var_init(&args);
    amxc_var_init(&ret);

    print_message("remove interface br-lan\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    var = amxc_var_add_key(amxc_htable_t, &args, "cache", NULL);
    amxc_var_add_key(uint32_t, var, "ttl_max", 80000);
    amxc_var_add_key(uint32_t, var, "ttl_min", 600);
    amxc_var_add_key(uint32_t, var, "cache_size", 4000);
    print_message("expectation: subproc should be restarted\n");
    rv = amxm_execute_function("mod", "dns", "set-cache", &args, &ret);
    assert_int_equal(rv, 0);
    amxut_timer_go_to_future_ms(500); // start delay
    assert_int_equal(count + 1, ut_get_number_of_subproc_restarts());
    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_flush_cache_while_unbound_is_running(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;
    uint32_t count = ut_get_number_of_subproc_restarts();
    amxc_var_t uc_ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_init(&uc_ret);
    amxc_var_set(cstring_t, &uc_ret, "ok removed 12 rrsets, 10 messages and 0 key entries");

    print_message("call FlushCache\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    expect_check(_UnboundControl, args, args_equal_check, "flush_zone .");
    will_return(_UnboundControl, &uc_ret);
    rv = amxm_execute_function("mod", "dns", "flush-cache", &args, &ret);
    assert_int_equal(rv, 0);
    assert_int_equal(count, ut_get_number_of_subproc_restarts());
    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&uc_ret);
}

void test_server_failure(UNUSED void** state) {
    amxc_var_t data;
    amxd_object_t* object = amxd_dm_findf(dm, "Unbound.");
    const int servfail = 2;

    assert_non_null(object);
    assert_true(amxp_subproc_is_running(NULL));

    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &data, "rcode", servfail);
    amxc_var_add_key(cstring_t, &data, "qname", "test.example");
    amxc_var_add_key(cstring_t, &data, "addr", "127.0.0.1");
    amxd_object_emit_signal(object, "unbound:client-response", &data);
    expect_check(ut_server_failure, args, server_failure_equal_check, "flush_zone .");
    handle_events();

    amxc_var_clean(&data);
}

void test_enable_changed(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_true(amxp_subproc_is_running(NULL));

    print_message("set enable false\n");
    amxc_var_set(bool, &args, false);
    print_message("expectation: subproc should stop\n");
    rv = amxm_execute_function("mod", "dns", "enable-changed", &args, &ret);
    assert_int_equal(rv, 0);
    assert_false(amxp_subproc_is_running(NULL));

    print_message("set enable true\n");
    amxc_var_set(bool, &args, true);
    print_message("expectation: subproc should start\n");
    rv = amxm_execute_function("mod", "dns", "enable-changed", &args, &ret);
    assert_int_equal(rv, 0);
    amxut_timer_go_to_future_ms(500); // start delay
    assert_true(amxp_subproc_is_running(NULL));

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

static void trigger_rebind_changed_event(bool enable, csv_string_t ip_exceptions, csv_string_t domain_exceptions) {
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_new_key_bool(&args, "Enable", enable);
    amxc_var_add_new_key_csv_string_t(&args, "DomainExceptions", ip_exceptions);
    amxc_var_add_new_key_csv_string_t(&args, "IPExceptions", domain_exceptions);

    rv = amxm_execute_function("mod", "dns", "rebind-changed", &args, &ret);
    assert_int_equal(rv, 0);

    amxut_timer_go_to_future_ms(500); // start delay

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    handle_events();
}

static bool config_contains_string(cstring_t needle) {
    bool found = false;
    FILE* fp = fopen(DDNS_UNBOUND_CONFIG_FILE, "r");
    char line[512];
    amxc_string_t str;

    amxc_string_init(&str, 0);

    while(fgets(line, sizeof(line), fp)) {
        amxc_string_set(&str, line);
        if(amxc_string_search(&str, needle, 0) != -1) {
            found = true;
            break;
        }
    }
    fclose(fp);
    amxc_string_clean(&str);
    return found;
}

static void assert_config_contains_basic_rebind_protection() {
    assert_true(config_contains_string("    private-address: 10.0.0.0/8"));
    assert_true(config_contains_string("    private-address: 172.16.0.0/12"));
    assert_true(config_contains_string("    private-address: 192.168.0.0/16"));
    assert_true(config_contains_string("    private-address: fd00::/8"));
    assert_true(config_contains_string("    private-address: 169.254.0.0/16"));
    assert_true(config_contains_string("    private-address: fe80::/10"));
    assert_true(config_contains_string("    private-address: ::ffff:0:0/96"));
}

static void assert_config_does_not_contains_basic_rebind_protection() {
    assert_false(config_contains_string("    private-address: 10.0.0.0/8"));
    assert_false(config_contains_string("    private-address: 172.16.0.0/12"));
    assert_false(config_contains_string("    private-address: 192.168.0.0/16"));
    assert_false(config_contains_string("    private-address: fd00::/8"));
    assert_false(config_contains_string("    private-address: 169.254.0.0/16"));
    assert_false(config_contains_string("    private-address: fe80::/10"));
    assert_false(config_contains_string("    private-address: ::ffff:0:0/96"));
}

void test_rebind_changed(UNUSED void** state) {
    print_message("expectation: rebind protection is enabled by default\n");
    assert_config_contains_basic_rebind_protection();
    assert_false(config_contains_string("    private-domain: 192.168.18.0/24"));
    assert_false(config_contains_string("    private-domain: 192.168.5.0/24"));
    assert_false(config_contains_string("    private-exception: example.com"));
    assert_false(config_contains_string("    private-exception: example.org"));
    assert_true(amxp_subproc_is_running(NULL));

    print_message("turn rebind protection off\n");
    trigger_rebind_changed_event(false, "", "");

    print_message("expectation: rebind protection is disabled\n");
    assert_config_does_not_contains_basic_rebind_protection();
    assert_false(config_contains_string("    private-domain: 192.168.18.0/24"));
    assert_false(config_contains_string("    private-domain: 192.168.5.0/24"));
    assert_false(config_contains_string("    private-exception: example.com"));
    assert_false(config_contains_string("    private-exception: example.org"));
    assert_true(amxp_subproc_is_running(NULL));

    print_message("turn rebind protection on with exceptions\n");
    trigger_rebind_changed_event(true, "192.168.18.0/24,192.168.5.0/24", "example.com,example.org");

    print_message("expectation: rebind protection is enabled with exceptions\n");
    assert_config_contains_basic_rebind_protection();
    assert_true(config_contains_string("    private-domain: 192.168.18.0/24"));
    assert_true(config_contains_string("    private-domain: 192.168.5.0/24"));
    assert_true(config_contains_string("    private-exception: example.com"));
    assert_true(config_contains_string("    private-exception: example.org"));
    assert_true(amxp_subproc_is_running(NULL));
}

void test_remove_local_host_address_while_unbound_is_running(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* var = NULL;
    int rv = -1;
    uint32_t count = ut_get_number_of_subproc_restarts();
    amxc_var_t uc_ret1;
    amxc_var_t uc_ret2;
    amxc_var_t uc_ret3;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_init(&uc_ret1);
    amxc_var_set(cstring_t, &uc_ret1, "ok");

    amxc_var_init(&uc_ret2);
    amxc_var_set(cstring_t, &uc_ret2, "Adr-s-A55. 3600    IN      A       192.168.30.11\nprplOS.home.arpa.	3600	IN	A	192.168.30.1\n");

    amxc_var_init(&uc_ret3);
    amxc_var_set(cstring_t, &uc_ret3, "added 2 datas");

    print_message("remove host address\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", "mytvbox");
    amxc_var_add_key(cstring_t, &args, "interface", "br-lan");
    var = amxc_var_add_key(amxc_llist_t, &args, "ipv4_addresses", NULL);
    amxc_var_add(cstring_t, var, "192.168.1.12");
    var = amxc_var_add_key(amxc_llist_t, &args, "ipv6_addresses", NULL);
    amxc_var_add(cstring_t, var, "fe80::12");

    print_message("expectation: subproc should not be restarted for removing a host address\n");
    expect_check(_UnboundControl, args, args_equal_check, "view_local_data_remove br-lan mytvbox");
    will_return(_UnboundControl, &uc_ret1);
    expect_check(_UnboundControl, args, args_equal_check, "view_local_data_remove br-lan mytvbox.home");
    will_return(_UnboundControl, &uc_ret1);
    expect_check(_UnboundControl, args, args_equal_check, "view_list_local_data br-lan");
    will_return(_UnboundControl, &uc_ret2);
    expect_check(_UnboundControl, args, args_equal_check,
                 "view_local_datas2 br-lan,"
                 "mytvbox IN A 192.168.1.11,"
                 "mytvbox.home IN A 192.168.1.11,"
                 "mytvbox IN AAAA fe80::11,"
                 "mytvbox.home IN AAAA fe80::11");
    will_return(_UnboundControl, &uc_ret3);
    rv = amxm_execute_function("mod", "dns", "remove-host-address", &args, &ret);
    assert_int_equal(rv, 0);
    assert_int_equal(count, ut_get_number_of_subproc_restarts());

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&uc_ret1);
    amxc_var_clean(&uc_ret2);
    amxc_var_clean(&uc_ret3);
}

void test_remove_local_host_while_unbound_is_running(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t uc_ret;
    int rv = -1;
    uint32_t count = ut_get_number_of_subproc_restarts();

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_init(&uc_ret);
    amxc_var_set(cstring_t, &uc_ret, "ok");

    print_message("remove host\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", "mytvbox");
    amxc_var_add_key(cstring_t, &args, "interface", "br-lan");

    print_message("expectation: subproc should not be restarted\n");
    expect_check(_UnboundControl, args, args_equal_check, "view_local_data_remove br-lan mytvbox");
    will_return(_UnboundControl, &uc_ret);
    expect_check(_UnboundControl, args, args_equal_check, "view_local_data_remove br-lan mytvbox.home");
    will_return(_UnboundControl, &uc_ret);
    rv = amxm_execute_function("mod", "dns", "remove-host", &args, &ret);
    assert_int_equal(rv, 0);
    assert_int_equal(count, ut_get_number_of_subproc_restarts());

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&uc_ret);
}

void test_remove_external_host_while_unbound_is_running(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t uc_ret;
    int rv = -1;
    uint32_t count = ut_get_number_of_subproc_restarts();

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_init(&uc_ret);
    amxc_var_set(cstring_t, &uc_ret, "ok");

    print_message("remove host\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", "example.com");
    amxc_var_add_key(cstring_t, &args, "interface", "br-lan");

    print_message("expectation: subproc should not be restarted\n");
    expect_check(_UnboundControl, args, args_equal_check, "view_local_data_remove br-lan example.com");
    will_return(_UnboundControl, &uc_ret);
    rv = amxm_execute_function("mod", "dns", "remove-host", &args, &ret);
    assert_int_equal(rv, 0);
    assert_int_equal(count, ut_get_number_of_subproc_restarts());

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&uc_ret);
}

void test_remove_interface(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t uc_ret;
    int rv = -1;
    uint32_t count = ut_get_number_of_subproc_restarts();

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_init(&uc_ret);
    amxc_var_set(cstring_t, &uc_ret, "no view with name: br-lan");

    print_message("remove interface br-lan\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "interface", "br-lan");
    print_message("expectation: subproc should not be restarted\n");
    expect_check(_UnboundControl, args, args_equal_check, "interface_remove br-lan");
    will_return(_UnboundControl, &uc_ret);
    rv = amxm_execute_function("mod", "dns", "remove-interface", &args, &ret);
    assert_int_equal(rv, 0);
    amxut_timer_go_to_future_ms(500); // start delay
    assert_int_equal(count, ut_get_number_of_subproc_restarts());

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&uc_ret);
}

void test_remove_server_while_unbound_is_running(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t uc_ret;
    int rv = -1;
    uint32_t count = ut_get_number_of_subproc_restarts();

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_init(&uc_ret);
    amxc_var_set(cstring_t, &uc_ret, "ok");

    print_message("remove server srv-2\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", "srv-2");
    print_message("expectation: subproc should not be restarted\n");
    expect_check(_UnboundControl, args, args_equal_check, "forward_add . 8.8.8.8");
    will_return(_UnboundControl, &uc_ret);
    rv = amxm_execute_function("mod", "dns", "remove-server", &args, &ret);
    assert_int_equal(rv, 0);
    assert_int_equal(count, ut_get_number_of_subproc_restarts());

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&uc_ret);
}

void test_add_fwzone(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;
    amxc_var_t uc_ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_init(&uc_ret);
    amxc_var_set(cstring_t, &uc_ret, "ok");

    print_message("add server srvg-1 to forward zone zone1.com\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", "srvg-1");
    amxc_var_add_key(cstring_t, &args, "server", "8.8.8.8");
    amxc_var_add_key(cstring_t, &args, "zone_name", "zone1.com");
    amxc_var_add_key(bool, &args, "enable", true);
    expect_check(_UnboundControl, args, args_equal_check, "forward_add zone1.com 8.8.8.8");
    will_return(_UnboundControl, &uc_ret);
    rv = amxm_execute_function("mod", "dns", "add-server", &args, &ret);
    assert_int_equal(rv, 0);
    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&uc_ret);
}

void test_remove_fwzone(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;
    amxc_var_t uc_ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_init(&uc_ret);
    amxc_var_set(cstring_t, &uc_ret, "ok");

    print_message("remove all servers from zone zone1.com\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", "srvg-1");
    amxc_var_add_key(cstring_t, &args, "zone_name", "zone1.com");
    expect_check(_UnboundControl, args, args_equal_check, "forward_remove zone1.com");
    will_return(_UnboundControl, &uc_ret);
    rv = amxm_execute_function("mod", "dns", "remove-server", &args, &ret);
    assert_int_equal(rv, 0);
    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&uc_ret);
}