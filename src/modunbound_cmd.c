/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxp/amxp_dir.h>
#include <amxd/amxd_dm.h>
#include <amxm/amxm.h>
#include <amxb/amxb.h>
#include <amxo/amxo.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "modunbound.h"
#include "modunbound_cmd.h"

#define IGNORE_VALIDATION_ERROR true
#define THROW_VALIDATION_ERROR false
#define INTERFACE_LIST_POSITION 6000 //Big number in order to add at the end of the interface list. Usually the list is maximum 4, just put 6000 to be sure, no specific reason for the number.

//a local hostname will have the lan domain name so 2 records are added
bool is_local_hostname_base(const char* address) {
    if(string_empty(address)) {
        return false;
    }
    return strpbrk(address, ".:") == NULL;
}

unbound_status_t cmd_set_server_parameters(int timeout, int repeat, const char* zone_name) {
    unbound_status_t status = unbound_other_error;
    amxc_string_t cmd;

    amxc_string_init(&cmd, 0);
    amxc_string_setf(&cmd, "timeout_repeat %d %d", timeout, repeat);

    status = unbound_call_ok(amxc_string_get(&cmd, 0));
    when_failed_trace(status, exit, ERROR, "failed to update timeout_repeat (zone=%s)", zone_name);

exit:
    amxc_string_clean(&cmd);
    return status;
}

unbound_status_t cmd_add_interface(const char* interface) {
    unbound_status_t status = unbound_other_error;
    amxc_var_t str_list;
    amxc_string_t cmd;

    amxc_string_init(&cmd, 0);
    amxc_var_init(&str_list);
    amxc_var_set_type(&str_list, AMXC_VAR_ID_LIST);

    when_str_empty(interface, exit);

    amxc_string_setf(&cmd, "interface %s,%d", interface, INTERFACE_LIST_POSITION);
    amxc_var_add(cstring_t, &str_list, "");

    status = unbound_call_expected(amxc_string_get(&cmd, 0), &str_list, THROW_VALIDATION_ERROR);
    when_failed_trace(status, exit, ERROR, "failed to add interface '%s'", interface);

exit:
    amxc_string_clean(&cmd);
    amxc_var_clean(&str_list);
    return status;
}

unbound_status_t cmd_remove_interface(const char* interface) {
    unbound_status_t status = unbound_other_error;
    amxc_var_t str_list;
    amxc_string_t cmd;

    amxc_string_init(&cmd, 0);
    amxc_var_init(&str_list);
    amxc_var_set_type(&str_list, AMXC_VAR_ID_LIST);

    when_str_empty(interface, exit);

    amxc_string_setf(&cmd, "interface_remove %s", interface);
    amxc_var_add(cstring_t, &str_list, "");
    amxc_var_add(cstring_t, &str_list, "no view with name:");
    amxc_var_add(cstring_t, &str_list, "failed to delete the view with name:");

    status = unbound_call_expected(amxc_string_get(&cmd, 0), &str_list, THROW_VALIDATION_ERROR);
    when_failed_trace(status, exit, ERROR, "failed to remove interface '%s'", interface);

exit:
    amxc_string_clean(&cmd);
    amxc_var_clean(&str_list);
    return status;
}

static bool cmd_check_view_exists(const char* interface) {
    amxc_string_t cmd;
    amxc_var_t str_list;
    bool exists = true;

    amxc_string_init(&cmd, 0);
    amxc_var_init(&str_list);
    amxc_var_set_type(&str_list, AMXC_VAR_ID_LIST);

    when_str_empty(interface, exit);

    amxc_string_setf(&cmd, "view_list_local_data %s", interface);
    amxc_var_add(cstring_t, &str_list, "no view with name:");

    when_failed(unbound_call_expected(amxc_string_get(&cmd, 0), &str_list, IGNORE_VALIDATION_ERROR), exit);

    exists = false;
exit:
    amxc_string_clean(&cmd);
    amxc_var_clean(&str_list);
    return exists;
}

unbound_status_t cmd_add_view(const char* interface) {
    unbound_status_t status = unbound_other_error;
    amxc_var_t str_list;
    amxc_string_t cmd;

    amxc_string_init(&cmd, 0);
    amxc_var_init(&str_list);
    amxc_var_set_type(&str_list, AMXC_VAR_ID_LIST);

    when_str_empty(interface, exit);

    amxc_string_setf(&cmd, "view %s,yes", interface);
    amxc_var_add(cstring_t, &str_list, "view added:");

    status = unbound_call_expected(amxc_string_get(&cmd, 0), &str_list, THROW_VALIDATION_ERROR);
    when_failed_trace(status, exit, ERROR, "failed to add view '%s'", interface);

exit:
    amxc_string_clean(&cmd);
    amxc_var_clean(&str_list);
    return status;
}

unbound_status_t cmd_add_host(const char* interface, const char* hostname, const char* domain_name,
                              amxc_var_t* ipv4_addresses, amxc_var_t* ipv6_addresses, bool accept_empty_address) {
    unbound_status_t status = unbound_other_error;
    bool has_addresses = false;
    amxc_var_t str_list;
    amxc_string_t cmd;
    bool is_local = is_local_hostname_base(hostname);

    amxc_string_init(&cmd, 0);
    amxc_var_init(&str_list);
    amxc_var_set_type(&str_list, AMXC_VAR_ID_LIST);

    when_str_empty(interface, exit);
    when_str_empty(hostname, exit);

    has_addresses = (NULL != amxc_var_get_first(ipv4_addresses)) || (NULL != amxc_var_get_first(ipv6_addresses));
    if(!accept_empty_address) {
        when_false(has_addresses, exit);
    } else {
        when_false_status(has_addresses, exit, status = unbound_ok);
    }

    amxc_string_setf(&cmd, "view_local_datas2 %s", interface);
    amxc_var_for_each(var, ipv4_addresses) {
        const char* ip_address = GET_CHAR(var, NULL);
        amxc_string_appendf(&cmd, ",%s IN A %s", hostname, ip_address);
        if(is_local && !string_empty(domain_name)) {
            amxc_string_appendf(&cmd, ",%s.%s IN A %s", hostname, domain_name, ip_address);
        }
    }
    amxc_var_for_each(var, ipv6_addresses) {
        const char* ip_address = GET_CHAR(var, NULL);
        amxc_string_appendf(&cmd, ",%s IN AAAA %s", hostname, ip_address);
        if(is_local && !string_empty(domain_name)) {
            amxc_string_appendf(&cmd, ",%s.%s IN AAAA %s", hostname, domain_name, ip_address);
        }
    }

    if(!cmd_check_view_exists(interface)) {
        status = cmd_add_view(interface);
        when_failed_trace(status, exit, ERROR, "failed to add view before adding host(s)");
    }
    amxc_var_add(cstring_t, &str_list, "added");

    status = unbound_call_expected(amxc_string_get(&cmd, 0), &str_list, THROW_VALIDATION_ERROR);
    when_failed_trace(status, exit, ERROR, "failed to add host(s)");

exit:
    amxc_string_clean(&cmd);
    amxc_var_clean(&str_list);
    return status;
}

static int cmd_view_local_data_remove(const char* interface, const char* hostname, const char* domain_name) {
    unbound_status_t status = unbound_other_error;
    amxc_string_t cmd;

    amxc_string_init(&cmd, 0);

    when_str_empty(interface, exit);
    when_str_empty(hostname, exit);

    amxc_string_setf(&cmd, "view_local_data_remove %s %s", interface, hostname);
    if(!string_empty(domain_name)) {
        amxc_string_appendf(&cmd, ".%s", domain_name);
    }

    status = unbound_call_ok(amxc_string_get(&cmd, 0));
    when_failed_trace(status, exit, ERROR, "failed to remove host(s)");

exit:
    amxc_string_clean(&cmd);
    return status;
}

unbound_status_t cmd_remove_host(const char* interface, const char* hostname, const char* domain_name) {
    unbound_status_t status = unbound_other_error;
    bool is_local = is_local_hostname_base(hostname);

    status = cmd_view_local_data_remove(interface, hostname, NULL);
    when_failed(status, exit);

    if(is_local && !string_empty(domain_name)) {
        cmd_view_local_data_remove(interface, hostname, domain_name);
    }

exit:
    return status;
}

unbound_status_t cmd_forward_set(amxc_var_t* servers, const char* zone_name) {
    unbound_status_t status = unbound_other_error;
    amxc_string_t cmd;

    amxc_string_init(&cmd, 0);

    when_str_empty(zone_name, exit);

    if(amxc_var_get_first(servers) != NULL) {
        amxc_string_setf(&cmd, "forward_add %s", zone_name);
        amxc_var_for_each(var, servers) {
            amxc_string_appendf(&cmd, " %s", GET_CHAR(var, "server"));
        }
    } else {
        amxc_string_setf(&cmd, "forward_remove %s", zone_name);
    }

    status = unbound_call_ok(amxc_string_get(&cmd, 0));
    when_failed_trace(status, exit, ERROR, "failed to update forwarder(s) for zone (%s)", zone_name);

exit:
    amxc_string_clean(&cmd);
    return status;
}

unbound_status_t cmd_flush_cache(void) {
    return unbound_call_ok("flush_zone .");
}