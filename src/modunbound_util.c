/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxm/amxm.h>
#include <amxb/amxb.h>
#include <amxo/amxo.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "modunbound.h"

typedef enum {
    expects = 0,
    contains
} validate_t;

amxb_bus_ctx_t* get_unbound_ctx(void) {
    return amxb_be_who_has(UNBOUND_OBJECT_NAME);
}

static unbound_status_t unbound_call(const char* cmd, amxc_var_t* ret) {
    unbound_status_t status = unbound_ctx_error;
    int rv = -1;
    int compare_result = -1;
    amxc_var_t args;
    amxc_var_t* ret_copy = NULL;
    const char* result = NULL;
    amxb_bus_ctx_t* ctx = get_unbound_ctx();
    bool verbose = sahTraceZoneLevel(sahTraceGetZone(ME)) >= TRACE_LEVEL_INFO;

    amxc_var_init(&args);
    amxc_var_new(&ret_copy);

    when_null_trace(ctx, exit, ERROR, "Unexpected empty bus context for %s", UNBOUND_OBJECT_NAME);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "cmd", cmd);

    if(verbose) {
        SAH_TRACEZ_INFO(ME, "call UnboundControl");
        amxc_var_log(&args);
    }

    amxc_var_copy(ret_copy, ret);
    status = unbound_amxb_error;
    rv = amxb_call(ctx, UNBOUND_OBJECT_NAME, "UnboundControl", &args, ret, 5);
    when_failed_trace(rv, exit, ERROR, "failed amxb_call %d", rv);

    if(verbose) {
        SAH_TRACEZ_INFO(ME, "retval of UnboundControl");
        amxc_var_log(ret);
    }

    status = unbound_ret_error;
    if(amxc_var_compare(ret, ret_copy, &compare_result) == 0) {
        when_true(compare_result == 0, exit);
    }

    status = unbound_validation_error;
    result = GETI_CHAR(ret, 0);
    when_null(result, exit);

    status = unbound_ok;
exit:
    amxc_var_clean(&args);
    amxc_var_delete(&ret_copy);
    return status;
}

static unbound_status_t _unbound_call_validate_string(const char* cmd, amxc_var_t* list_strings, bool ignore_validation_error, validate_t type) {
    amxc_var_t ret;
    amxc_string_t ret_without_nl;
    unbound_status_t status = unbound_other_error;
    const char* result = NULL;
    bool matched_string = false;

    amxc_var_init(&ret);
    amxc_string_init(&ret_without_nl, 0);

    status = unbound_call(cmd, &ret);
    when_false(status == unbound_ok, exit);
    amxc_string_setf(&ret_without_nl, "%s", GETI_CHAR(&ret, 0));
    amxc_string_replace(&ret_without_nl, "\n", "", UINT32_MAX);
    result = amxc_string_get(&ret_without_nl, 0);

    if(list_strings == NULL) {
        matched_string = true;
    } else {
        when_null(result, exit);
    }

    status = unbound_validation_error;
    amxc_var_for_each(string, list_strings) {
        const char* expected_string = GET_CHAR(string, NULL);
        int length = strlen(expected_string);
        bool ok = false;

        if(string_empty(expected_string)) {
            if(string_empty(result)) {
                ok = true;
            }
        } else {
            switch(type) {
            case expects:
                ok = (strncmp(result, expected_string, length) == 0);
                break;
            case contains:
                ok = (strstr(result, expected_string) != NULL);
                break;
            default:
                break;
            }
        }
        if(!ok) {
            if(!ignore_validation_error) {
                SAH_TRACEZ_INFO(ME, "unexpected return value '%s' - it expected a string '%s'", result != NULL ? result : "empty retval", expected_string);
            } else {
                continue;
            }
        } else {
            matched_string = true;
            break;
        }

    }

    if(!ignore_validation_error) {
        when_false_trace(matched_string, exit, ERROR, "Failed to match the expected value with '%s'", result != NULL ? result : "empty retval");
    } else {
        when_false(matched_string, exit);
    }

    status = unbound_ok;
exit:
    amxc_var_clean(&ret);
    amxc_string_clean(&ret_without_nl);
    return status;
}


static unbound_status_t unbound_call_expected_string(const char* cmd, amxc_var_t* list_strings, bool ignore_validation_error) {
    return _unbound_call_validate_string(cmd, list_strings, ignore_validation_error, expects);
}

static unbound_status_t unbound_call_contains_string(const char* cmd, amxc_var_t* list_strings, bool ignore_validation_error) {
    return _unbound_call_validate_string(cmd, list_strings, ignore_validation_error, contains);
}

unbound_status_t unbound_call_ok(const char* cmd) {
    unbound_status_t status = unbound_other_error;
    amxc_var_t str_list;
    amxc_var_init(&str_list);
    amxc_var_set_type(&str_list, AMXC_VAR_ID_LIST);

    amxc_var_add(cstring_t, &str_list, "ok");
    status = unbound_call_expected_string(cmd, &str_list, true);
    amxc_var_clean(&str_list);
    return status;
}

unbound_status_t unbound_call_expected(const char* cmd, amxc_var_t* list_strings, bool ignore_validation_error) {
    return unbound_call_expected_string(cmd, list_strings, ignore_validation_error);
}

unbound_status_t unbound_call_contains(const char* cmd, amxc_var_t* list_strings, bool ignore_validation_error) {
    return unbound_call_contains_string(cmd, list_strings, ignore_validation_error);
}

