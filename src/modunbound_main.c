/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxp/amxp_dir.h>
#include <amxd/amxd_dm.h>
#include <amxm/amxm.h>
#include <amxb/amxb.h>
#include <amxo/amxo.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "modunbound.h"
#include "modunbound_cmd.h"

#define DELAY_TIME 500
#define WAIT_FOR_CHILD_IN_SEC 5
#define MODUNBOUND_SERVFAIL 2
#ifndef DNS_UNBOUND_CONFIG_FILE
#define DNS_UNBOUND_CONFIG_FILE "/etc/unbound/unbound.conf"
#endif

typedef enum {
    STOPPED = 0,
    WAIT_START_TIMER,
    STARTING,
    RUNNING,
} state_t;

static const char* strstate(state_t state) {
    const char* states[] = {"STOPPED", "WAIT_TIMER", "STARTING", "RUNNING"};
    return state > RUNNING ? "unknown state" : states[state];
}

typedef struct {
    bool enable;
    amxc_var_t domains;
    amxc_var_t ips;
} globals_rebind_t;

typedef struct {
    bool enable;
    bool out_of_sync;
    amxc_var_t config;
    globals_rebind_t rebind;
    amxp_subproc_t* process;
    state_t state;
    amxp_timer_t* start_timer;
    uint32_t start_timer_in_ms;
    amxp_timer_t* wait_for_child;
    int32_t timeout;
    int32_t repeat;
} globals_t;

typedef struct {
    const char* text;
    bool logged;
} unbound_option_t;

static globals_t g;
static const char* list[] = {"ipv4_addresses", "ipv6_addresses", NULL};
static unbound_option_t unbound_opt[] = {
    {"outbound-msg-timeout", false},
    {NULL, false}
};

static void handle_unbound(const char* caller);
static bool stop_unbound(state_t next_state);

static void update_state(state_t state, const char* reason) {
    SAH_TRACEZ_INFO(ME, "state change %s -> %s timer %d (reason: %s)", strstate(g.state), strstate(state), amxp_timer_get_state(g.start_timer), reason);
    g.state = state;
}

static char* get_date(char* date, size_t date_size) {
    time_t t;
    struct tm result;
    struct tm* tm = NULL;

    t = time(NULL);
    if((t != -1) && ((tm = localtime_r(&t, &result)) != NULL)) {
        strftime(date, date_size, "%F %T", tm);
    } else {
        snprintf(date, date_size, "0000-00-00 00:00");
    }

    return date;
}

static void config_file_populate_private_addresses(FILE* file) {
    // Protect private addresses from rebind attacks
    when_false(g.rebind.enable, exit);

    // Site local address space
    fprintf(file, "    private-address: 10.0.0.0/8\n");     // IPv4 rfc1918
    fprintf(file, "    private-address: 172.16.0.0/12\n");  // IPv4 rfc1918
    fprintf(file, "    private-address: 192.168.0.0/16\n"); // IPv4 rfc1918
    fprintf(file, "    private-address: fd00::/8\n");       // IPv6

    // Link local address space
    fprintf(file, "    private-address: 169.254.0.0/16\n"); // IPv4
    fprintf(file, "    private-address: fe80::/10\n");      // IPv6

    // Device local
    //   leaving this enabled might hinder many spam-blocklists (RBL)!
    fprintf(file, "    private-address: 127.0.0.0/8\n");    // IPv4
    fprintf(file, "    private-address: ::1/128\n");        // IPv6

    // IPv4 ranges mapped on IPv6
    fprintf(file, "    private-address: ::ffff:0:0/96\n");

    // Domain exceptions
    amxc_var_for_each(domain, &g.rebind.domains) {
        fprintf(file, "    private-domain: %s\n", GET_CHAR(domain, NULL));
    }

    // IP exceptions
    amxc_var_for_each(ip, &g.rebind.ips) {
        fprintf(file, "    private-exception: %s\n", GET_CHAR(ip, NULL));
    }
exit:
    return;
}

static bool create_config_file(void) {
    amxc_var_t* interfaces = GET_ARG(&g.config, "interfaces");
    amxc_var_t* cache = GET_ARG(&g.config, "cache");
    amxc_var_t* options = GET_ARG(&g.config, "options");
    amxc_var_t* fwzones = GET_ARG(&g.config, "fwzones");
    FILE* file = fopen(DNS_UNBOUND_CONFIG_FILE ".new", "w");
    char date[64];
    int rv = -1;

    when_null_trace(file, exit, ERROR, "Could not open file " DNS_UNBOUND_CONFIG_FILE ".new for writing");

    fprintf(file, "# unbound configuration file, generated by tr181-dns on %s.\n", get_date(date, sizeof(date)));
    fprintf(file, "\n");

    fprintf(file, "server:\n");
    fprintf(file, "# ODL {\n");
    amxc_var_for_each(option, options) {
        int ignore = -1;
        // because of backward compatibility, it is possible that some options are set in out of date odl files
        for(int i = 0; unbound_opt[i].text != NULL; i++) {
            if((strstr(GET_CHAR(option, NULL), unbound_opt[i].text) != NULL) && (g.timeout != -1)) {
                if(!unbound_opt[i].logged) {
                    SAH_TRACEZ_WARNING(ME, "please remove option '%s' from ODL", unbound_opt[i].text);
                    unbound_opt[i].logged = true; // logging once is enough
                }
                ignore = i;
                break;
            }
        }
        fprintf(file, "%c   %s\n", (ignore >= 0) ? '#' : ' ', GET_CHAR(option, NULL)); // assume option is not already be configured by default
    }
    fprintf(file, "# }\n");
    fprintf(file, "    username: \"\"\n");
    fprintf(file, "    chroot: \"\"\n");
    fprintf(file, "    directory: \"/var/lib\"\n"); // otherwise unbound stops because /var/lib/unbound/ doesn't exist
    fprintf(file, "    do-daemonize: no\n");
    fprintf(file, "    module-config: \"iterator\"\n");
    fprintf(file, "    max-udp-size: 4096\n");
    if(cache != NULL) {
        uint32_t ttl_max = GET_UINT32(cache, "ttl_max");
        uint32_t ttl_min = GET_UINT32(cache, "ttl_min");
        uint32_t cache_size = GET_UINT32(cache, "cache_size");
        // for now use the cache-size for all caches instead of having some sort of distribution key
        fprintf(file, "    rrset-cache-size: %uk\n", cache_size);
        fprintf(file, "    msg-cache-size: %uk\n", cache_size);
        fprintf(file, "    key-cache-size: %uk\n", cache_size);
        fprintf(file, "    neg-cache-size: %uk\n", cache_size);
        if(ttl_max > 0) {
            fprintf(file, "    cache-max-ttl: %u\n", ttl_max);
        }
        if(ttl_min > 0) {
            fprintf(file, "    cache-min-ttl: %u\n", ttl_min);
        }
    }
    fprintf(file, "    infra-cache-numhosts: %u\n", 100);   // unbound.conf manpage says the default value is 10k which seems a bit much?
    fprintf(file, "    infra-keep-probing: yes\n");         // otherwise Unbound will stop (if delegation point with 1 address) after 1 retry and not all outbound-msg-retry
    fprintf(file, "    bind-interfaces: yes\n");
    fprintf(file, "    access-control: 0.0.0.0/0 allow\n"); // shoud be ok since using bind-interfaces
    fprintf(file, "    access-control: ::0/0 allow\n");     // shoud be ok since using bind-interfaces
    if(g.timeout != -1) {
        fprintf(file, "    outbound-msg-timeout: %d\n", g.timeout);
    }
    if(g.repeat != -1) {
        fprintf(file, "    outbound-msg-retry: %d\n", g.repeat);
    }
    if(NULL == GET_ARG(interfaces, "lo")) {                 // to avoid integration problems (requires tr181-dns to configure DNS.Relay.X_PRPL-COM_Config for loopback)
        fprintf(file, "    interface: lo\n");
    }
    amxc_var_for_each(var, interfaces) {
        const char* interface = amxc_var_key(var);
        if(GET_BOOL(var, "up")) {
            fprintf(file, "    interface: %s\n", interface);
        }
    }
    config_file_populate_private_addresses(file);
    fprintf(file, "\n");

    amxc_var_for_each(fwzone, fwzones) {
        fprintf(file, "forward-zone:\n");
        fprintf(file, "    name: \"%s\"\n", amxc_var_key(fwzone));
        amxc_var_for_each(fwaddr, fwzone) {
            fprintf(file, "    forward-addr: %s\n", GET_CHAR(fwaddr, "server"));
        }
        fprintf(file, "\n");
    }

    amxc_var_for_each(var, interfaces) {
        const char* view = amxc_var_key(var);
        const char* domain_name = GET_CHAR(var, "domain_name");
        fprintf(file, "view:\n");
        fprintf(file, "    name: \"%s\"\n", view);
        fprintf(file, "    view-first: yes  # also check global local-zones\n");
        amxc_var_for_each(host, GET_ARG(var, "hosts")) {
            const char* hostname = amxc_var_key(host);
            bool is_local = is_local_hostname_base(hostname);
            amxc_var_for_each(ipaddress, GET_ARG(host, "ipv4_addresses")) {
                fprintf(file, "    local-data: '%s A %s'\n", hostname, GET_CHAR(ipaddress, NULL));
                if(is_local && !string_empty(domain_name)) {
                    fprintf(file, "    local-data: '%s.%s A %s'\n", hostname, domain_name, GET_CHAR(ipaddress, NULL));
                }
            }
            amxc_var_for_each(ipaddress, GET_ARG(host, "ipv6_addresses")) {
                fprintf(file, "    local-data: '%s AAAA %s'\n", hostname, GET_CHAR(ipaddress, NULL));
                if(is_local && !string_empty(domain_name)) {
                    fprintf(file, "    local-data: '%s.%s AAAA %s'\n", hostname, domain_name, GET_CHAR(ipaddress, NULL));
                }
            }
        }
        fprintf(file, "\n");
    }

    fclose(file);
    rename(DNS_UNBOUND_CONFIG_FILE ".new", DNS_UNBOUND_CONFIG_FILE);

    rv = 0;

exit:
    return rv == 0;
}

static void update_forwarding_status(void) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* fwzones = GET_ARG(&g.config, "fwzones");

    amxc_var_init(&args);
    amxc_var_init(&ret);

    if(g.state == RUNNING) {
        amxc_var_copy(&args, fwzones);
    } else {
        amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    }

    amxm_execute_function(NULL, "core", "update-forwarding-status", &args, &ret);

    amxc_var_clean(&ret);
    amxc_var_clean(&args);
}

static void client_response_event(UNUSED const char* const sig_name,
                                  const amxc_var_t* const data,
                                  UNUSED void* const priv) {
    uint32_t rcode = GET_UINT32(data, "rcode");
    SAH_TRACEZ_INFO(ME, "unbound event received rcode=%u", rcode);
    if(rcode == MODUNBOUND_SERVFAIL) {
        amxc_var_t args;
        amxc_var_t ret;

        amxc_var_init(&args);
        amxc_var_init(&ret);

        amxm_execute_function(NULL, "core", "server-failure", &args, &ret);

        amxc_var_clean(&ret);
        amxc_var_clean(&args);
    }
}

static void unbound_stopped(UNUSED const char* const event_name,
                            const amxc_var_t* const data,
                            UNUSED void* const priv) {
    uint32_t exit_code = GET_UINT32(data, "ExitCode");

    if(exit_code != 0) { // this event handler is always called, even on expected restart, only handle the error cases with this function
        SAH_TRACEZ_ERROR(ME, "Unexpected unbound stop: %d", exit_code);
        update_state(STOPPED, __func__);
    } else {
        SAH_TRACEZ_INFO(ME, "Unbound stopped");
    }
    update_forwarding_status();
    amxp_timer_stop(g.wait_for_child);
}

static void unbound_wait_done(UNUSED const char* const s,
                              UNUSED const amxc_var_t* const d,
                              UNUSED void* const p) {
    amxb_bus_ctx_t* ctx = NULL;

    when_false_trace(g.state == STARTING, exit, ERROR, "ignored " WAIT_DONE_SIGNAL);

    amxp_timer_stop(g.wait_for_child);

    if(g.out_of_sync) {
        SAH_TRACEZ_INFO(ME, "Config changed while Unbound was starting, restart Unbound");
        stop_unbound(STOPPED);
        g.out_of_sync = false;
        handle_unbound(__func__); // I prefer to just start Unbound here BUT then the wait:done slot is never called so state is STARTING forever. Workarround is to let the timer start Unbound.
    } else {
        SAH_TRACEZ_INFO(ME, WAIT_DONE_SIGNAL " received");
        update_state(RUNNING, __func__);
        update_forwarding_status();
        ctx = get_unbound_ctx();
        when_null_trace(ctx, exit, ERROR, "Failed to subscribe on Unbound");
        amxb_subscribe(ctx, UNBOUND_OBJECT_NAME, "notification in ['unbound:client-response']", client_response_event, NULL);
    }

exit:
    return;
}

static bool stop_unbound(state_t next_state) {
    int rv = -1;

    if(!amxp_subproc_is_running(g.process)) {
        SAH_TRACEZ_INFO(ME, "Unbound already stopped");
        rv = 0;
        goto exit;
    }

    amxp_subproc_kill(g.process, SIGTERM);
    rv = amxp_subproc_wait(g.process, 1000);
    if(rv != 0) {
        SAH_TRACEZ_WARNING(ME, "Unbound won't stop, send SIGKILL");
        amxp_subproc_kill(g.process, SIGKILL);
        amxp_subproc_wait(g.process, 500);
    } else {
        SAH_TRACEZ_INFO(ME, "Stopping Unbound");
    }

    amxb_unsubscribe(get_unbound_ctx(), UNBOUND_OBJECT_NAME, client_response_event, NULL);
    update_state(next_state, __func__);

exit:
    return rv == 0;
}

static bool start_unbound(void) {
    const char* cmd[] = {"unbound", "-c", DNS_UNBOUND_CONFIG_FILE, NULL};
    int rv = -1;
    static uint32_t count = 0;

    if(amxp_subproc_is_running(g.process)) {
        SAH_TRACEZ_INFO(ME, "Unbound already running");
        rv = 0;
        goto exit;
    }

    rv = amxp_subproc_vstart(g.process, (char**) cmd);
    when_false_trace(rv == 0, exit, ERROR, "Failed to start Unbound");

    count++;
    SAH_TRACEZ_INFO(ME, "Started Unbound (%u times)", count);

    SAH_TRACEZ_INFO(ME, "wait for " WAIT_DONE_SIGNAL);
    update_state(STARTING, __func__);
    amxb_wait_for_object(UNBOUND_OBJECT_NAME); // works async
    amxp_timer_start(g.wait_for_child, WAIT_FOR_CHILD_IN_SEC * 1000);

exit:
    return rv == 0;
}

static void delayed_start(UNUSED amxp_timer_t* local_timer, UNUSED void* priv) {
    start_unbound();
}

static void handle_unbound(const char* reason) {
    SAH_TRACEZ_INFO(ME, "handle state %s (enable: %d, synced: %d, reason: %s)", strstate(g.state), g.enable, !g.out_of_sync, reason);

    if(!g.enable) {
        if(g.state == WAIT_START_TIMER) {
            amxp_timer_stop(g.start_timer);
            update_state(STOPPED, __func__);
        } else if(g.state > STOPPED) {
            stop_unbound(STOPPED);
        }
    }

    // g.start_timer is used to handle burst of events needing restart of Unbound

    switch(g.state) {
    case STOPPED:
        amxp_timer_start(g.start_timer, g.start_timer_in_ms);
        update_state(WAIT_START_TIMER, __func__);
        g.out_of_sync = false;
        break;
    case WAIT_START_TIMER:
        amxp_timer_start(g.start_timer, g.start_timer_in_ms);
        break;
    case STARTING:
        g.out_of_sync = true;
        break;
    case RUNNING:
        stop_unbound(WAIT_START_TIMER);
        amxp_timer_start(g.start_timer, g.start_timer_in_ms);
    default:
        break;
    }
}

static void apply_server_cfg(int32_t timeout, int32_t repeat, bool changed) {
    when_false(changed, exit);

    if(timeout != INT32_MIN) {
        g.timeout = timeout;
    }
    if(repeat != INT32_MIN) {
        g.repeat = repeat;
    }
    create_config_file();

exit:
    return;
}

static void apply_forward_servers_cfg(const char* name, amxc_var_t* servers, amxc_var_t* new_servers, bool changed) {
    amxc_var_t* fwzones = GET_ARG(&g.config, "fwzones");

    when_false(changed, exit);

    if(servers == NULL) {
        when_null(new_servers, exit);
        if((fwzones != NULL) && !string_empty(name)) {
            servers = amxc_var_add_key(amxc_htable_t, fwzones, name, NULL);
            amxc_var_copy(servers, new_servers);
        }
    } else {
        if(new_servers == NULL) {
            amxc_var_delete(&servers);
        } else {
            amxc_var_copy(servers, new_servers);
        }

    }
    create_config_file();

exit:
    return;
}

/**
 * in: timeout, repeat, servers:{<unique id>:{server:<ip addr>}, ...}
 * if list not empty then update servers to match list exactly
 * if list is empty then remove all
 * if list is missing then don't update servers
 */
static int set_server(UNUSED const char* function_name,
                      amxc_var_t* args,
                      UNUSED amxc_var_t* ret) {
    unbound_status_t status = unbound_other_error;
    amxc_var_t* servers = NULL;
    amxc_var_t* new_servers = NULL;
    const char* zone_name = ".";
    int32_t new_timeout = INT32_MIN;
    int32_t new_repeat = INT32_MIN;
    int rv = -1;
    bool servers_changed = false;
    bool tr_changed = false;
    bool check_critical = false;

    servers = GET_ARG(GET_ARG(&g.config, "fwzones"), zone_name);
    when_null(servers, exit);

    new_servers = GET_ARG(args, "servers");
    if(new_servers != NULL) {
        SAH_TRACEZ_INFO(ME, "servers changed");
        servers_changed = true;
    }

    if(GET_ARG(args, "timeout") != NULL) {
        new_timeout = GET_INT32(args, "timeout");
        if(new_timeout == 0) {
            new_timeout = -1; // use Unbound's default value
        }
        if(g.timeout != new_timeout) {
            SAH_TRACEZ_INFO(ME, "timeout changed %d -> %d", g.timeout, new_timeout);
            tr_changed = true;
        } else {
            SAH_TRACEZ_INFO(ME, "timeout didn't change");
        }
    }

    if(GET_ARG(args, "repeat") != NULL) {
        new_repeat = GET_INT32(args, "repeat");
        if(g.repeat != new_repeat) {
            SAH_TRACEZ_INFO(ME, "repeat changed %d -> %d", g.repeat, new_repeat);
            tr_changed = true;
        } else {
            SAH_TRACEZ_INFO(ME, "repeat didn't change");
        }
    }

    when_false(tr_changed || servers_changed, exit);

    if(g.state == RUNNING) {
        if(tr_changed) {
            check_critical = true;
            status = cmd_set_server_parameters(new_timeout, new_repeat, zone_name);
            when_failed(status, exit);
        }
        apply_server_cfg(new_timeout, new_repeat, tr_changed);

        if(servers_changed) {
            check_critical = true;
            status = cmd_forward_set(new_servers, zone_name);
            when_failed(status, exit);
        }
        apply_forward_servers_cfg(zone_name, servers, new_servers, servers_changed);
    } else {
        apply_server_cfg(new_timeout, new_repeat, tr_changed);
        apply_forward_servers_cfg(zone_name, servers, new_servers, servers_changed);
        if(g.enable) {
            handle_unbound(__func__);
        }
    }

    rv = 0;

exit:
    if(check_critical && (status != unbound_ok) && (status < unbound_other_error)) {
        SAH_TRACEZ_ERROR(ME, "Critical Error: Unbound will restart.");
        apply_server_cfg(new_timeout, new_repeat, tr_changed);
        apply_forward_servers_cfg(zone_name, servers, new_servers, servers_changed);
        handle_unbound(__func__);
    }
    amxc_var_delete(&new_servers);
    return rv;
}

static int apply_server_changes(amxc_var_t* servers, amxc_var_t* new_servers, const char* zone_name) {
    unbound_status_t status = unbound_other_error;
    int rv = -1;
    bool check_critical = false;

    if(g.state == RUNNING) {
        check_critical = true;
        status = cmd_forward_set(new_servers, zone_name);
        when_failed(status, exit);
        apply_forward_servers_cfg(zone_name, servers, new_servers, true);
        update_forwarding_status();
    } else {
        apply_forward_servers_cfg(zone_name, servers, new_servers, true);
        if(g.enable) {
            handle_unbound(__func__);
        }
    }

    rv = 0;

exit:
    if(check_critical && (status != unbound_ok) && (status < unbound_other_error)) {
        SAH_TRACEZ_ERROR(ME, "Critical Error: Unbound will restart.");
        apply_forward_servers_cfg(zone_name, servers, new_servers, true);
        handle_unbound(__func__);
    }
    return rv;
}

static int add_server(UNUSED const char* function_name,
                      amxc_var_t* args,
                      UNUSED amxc_var_t* ret) {
    amxc_var_t* servers = NULL;
    amxc_var_t* new_servers = NULL;
    amxc_var_t* subvar = NULL;
    const char* key = GET_CHAR(args, "name");
    const char* zone_name = GET_CHAR(args, "zone_name") == NULL ? "." : GET_CHAR(args, "zone_name");
    amxc_var_t* fwzones = GET_ARG(&g.config, "fwzones");
    int rv = -1;

    amxc_var_new(&new_servers);

    servers = GET_ARG(fwzones, zone_name);
    if(servers == NULL) {
        amxc_var_set_type(new_servers, AMXC_VAR_ID_HTABLE);
    } else {
        amxc_var_copy(new_servers, servers);
    }

    when_null(new_servers, exit);
    when_str_empty_trace(key, exit, ERROR, "Can't add server without name");
    when_false_trace(GET_ARG(new_servers, key) == NULL, exit, ERROR, "Server %s already in use", key);

    subvar = amxc_var_add_key(amxc_htable_t, new_servers, key, NULL);
    amxc_var_move(subvar, args);

    SAH_TRACEZ_INFO(ME, "Server '%s' added", GET_CHAR(subvar, "server"));
    rv = apply_server_changes(servers, new_servers, zone_name);

exit:
    amxc_var_delete(&new_servers);
    return rv;
}

static int remove_server(UNUSED const char* function_name,
                         amxc_var_t* args,
                         UNUSED amxc_var_t* ret) {
    amxc_var_t* servers = NULL;
    amxc_var_t* new_servers = NULL;
    amxc_var_t* subvar = NULL;
    const char* key = GET_CHAR(args, "name");
    const char* zone_name = GET_CHAR(args, "zone_name") == NULL ? "." : GET_CHAR(args, "zone_name");
    amxc_var_t* fwzones = GET_ARG(&g.config, "fwzones");
    int rv = -1;

    amxc_var_new(&new_servers);

    servers = GET_ARG(fwzones, zone_name);
    when_null(servers, exit);
    amxc_var_copy(new_servers, servers);

    when_str_empty_trace(key, exit, ERROR, "Can't remove server without name");
    subvar = GET_ARG(new_servers, key);
    when_null_trace(subvar, exit, INFO, "No server '%s' to remove", key);

    amxc_var_delete(&subvar);

    if(amxc_var_get_first(new_servers) == NULL) {
        amxc_var_delete(&new_servers);
        new_servers = NULL;
    }

    rv = apply_server_changes(servers, new_servers, zone_name);
exit:
    amxc_var_delete(&new_servers);
    return rv;
}

static int enable_changed(UNUSED const char* function_name,
                          amxc_var_t* args,
                          UNUSED amxc_var_t* ret) {
    g.enable = GET_BOOL(args, NULL);

    SAH_TRACEZ_INFO(ME, "Enable changed to %s (state=%s(%d))", g.enable ? "true" : "false", strstate(g.state), g.state);

    handle_unbound(__func__);

    return 0;
}

static int rebind_protection_changed(UNUSED const char* function_name,
                                     amxc_var_t* args,
                                     UNUSED amxc_var_t* ret) {
    int rv = -1;
    g.rebind.enable = GET_BOOL(args, "Enable");
    SAH_TRACEZ_INFO(ME, "Rebind protection changed (%sabled|%s|%s)",
                    g.rebind.enable ? "en" : "dis",
                    GET_CHAR(args, "DomainExceptions"),
                    GET_CHAR(args, "IPExceptions"));

    rv = amxc_var_convert(&g.rebind.domains, GET_ARG(args, "DomainExceptions"), AMXC_VAR_ID_LIST);
    when_failed_trace(rv, exit, ERROR, "Failed to convert(%d) %s", rv, "DomainExceptions");
    rv = amxc_var_convert(&g.rebind.ips, GET_ARG(args, "IPExceptions"), AMXC_VAR_ID_LIST);
    when_failed_trace(rv, exit, ERROR, "Failed to convert(%d) %s", rv, "IPExceptions");

    create_config_file();
    if(g.enable) {
        handle_unbound(__func__);
    }
exit:
    return rv;
}

static void apply_interface_cfg(amxc_var_t* interfaces, const char* interface, amxc_var_t* intf_var, amxc_var_t* new_intf) {
    if(intf_var == NULL) {
        when_null(new_intf, exit);
        if((interfaces != NULL) && !string_empty(interface)) {
            intf_var = amxc_var_add_key(amxc_htable_t, interfaces, interface, NULL);
            amxc_var_copy(intf_var, new_intf);
        }
    } else {
        if(new_intf == NULL) {
            amxc_var_delete(&intf_var);
        } else {
            amxc_var_copy(intf_var, new_intf);
        }
    }
    create_config_file();

exit:
    return;
}

static int set_interface(UNUSED const char* function_name,
                         amxc_var_t* args,
                         UNUSED amxc_var_t* ret) {
    unbound_status_t status = unbound_other_error;
    amxc_var_t* interfaces = GET_ARG(&g.config, "interfaces");
    amxc_var_t* intf = NULL;
    amxc_var_t* new_intf = NULL;
    const char* interface = GET_CHAR(args, "interface");
    bool check_critical = false;
    bool interface_down = GET_BOOL(args, "interface_down");

    amxc_var_new(&new_intf);

    intf = GET_ARG(interfaces, interface);
    if(intf == NULL) {
        SAH_TRACEZ_INFO(ME, "Add new interface '%s'", interface);
        amxc_var_set_type(new_intf, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(amxc_htable_t, new_intf, "hosts", NULL);
        amxc_var_add_key(bool, new_intf, "up", !interface_down);
    } else {
        amxc_var_copy(new_intf, intf);
        amxc_var_t* intf_up_arg = GET_ARG(new_intf, "up");
        bool intf_up = GET_BOOL(intf_up_arg, NULL);
        if(!interface_down && !intf_up) {
            SAH_TRACEZ_INFO(ME, "Changing interface '%s' up", interface);
            amxc_var_set(bool, intf_up_arg, true);
        } else {
            SAH_TRACEZ_INFO(ME, "Interface %s already known", interface);
            goto exit;
        }
    }

    if(g.state == RUNNING) {
        if(GET_BOOL(new_intf, "up")) {
            check_critical = true;
            status = cmd_add_interface(interface);
            when_failed(status, exit);
        }
        apply_interface_cfg(interfaces, interface, intf, new_intf);
    } else {
        apply_interface_cfg(interfaces, interface, intf, new_intf);
        if(g.enable) {
            handle_unbound(__func__);
        }
    }

exit:
    if(check_critical && (status != unbound_ok) && (status < unbound_other_error)) {
        SAH_TRACEZ_ERROR(ME, "Critical Error: Unbound will restart.");
        apply_interface_cfg(interfaces, interface, intf, new_intf);
        handle_unbound(__func__);
    }
    amxc_var_delete(&new_intf);
    return 0;
}

static int remove_interface(UNUSED const char* function_name,
                            amxc_var_t* args,
                            UNUSED amxc_var_t* ret) {
    unbound_status_t status = unbound_other_error;
    amxc_var_t* interfaces = GET_ARG(&g.config, "interfaces");
    amxc_var_t* intf = NULL;
    const char* interface = GET_CHAR(args, "interface");
    int rv = -1;
    bool check_critical = false;

    intf = GET_ARG(interfaces, interface);
    when_false_trace(intf != NULL, exit, INFO, "Interface '%s' unknown", interface);

    SAH_TRACEZ_INFO(ME, "Remove interface '%s'", interface);
    if(g.state == RUNNING) {
        if(GET_BOOL(intf, "up")) {
            check_critical = true;
            status = cmd_remove_interface(interface);
            when_failed(status, exit);
        }
        apply_interface_cfg(interfaces, interface, intf, NULL);
    } else {
        apply_interface_cfg(interfaces, interface, intf, NULL);
        if(g.enable) {
            handle_unbound(__func__);
        }
    }

    rv = 0;
exit:
    if(check_critical && (status != unbound_ok) && (status < unbound_other_error)) {
        SAH_TRACEZ_ERROR(ME, "Critical Error: Unbound will restart.");
        apply_interface_cfg(interfaces, interface, intf, NULL);
        handle_unbound(__func__);
    }
    return rv;
}

static void apply_host_cfg(amxc_var_t* hosts, const char* hostname, amxc_var_t* host, amxc_var_t* new_host) {
    if(host == NULL) {
        when_null(new_host, exit);
        if((hosts != NULL) && !string_empty(hostname)) {
            host = amxc_var_add_key(amxc_htable_t, hosts, hostname, NULL);
            amxc_var_copy(host, new_host);
        }
    } else {
        if(new_host == NULL) {
            amxc_var_delete(&host);
        } else {
            amxc_var_copy(host, new_host);
        }
    }
    create_config_file();

exit:
    return;
}

static int add_host(UNUSED const char* function_name,
                    amxc_var_t* args,
                    UNUSED amxc_var_t* ret) {
    unbound_status_t status = unbound_other_error;
    amxc_var_t* interfaces = GET_ARG(&g.config, "interfaces");
    amxc_var_t* interface_var = NULL;
    amxc_var_t* host = NULL;
    amxc_var_t* new_host = NULL;
    amxc_var_t* hosts = NULL;
    const char* interface = GET_CHAR(args, "interface");
    const char* hostname = GET_CHAR(args, "name");
    bool no_failover = GET_BOOL(args, "no_failover");
    int count = 0;
    int rv = -1;
    bool check_critical = false;

    amxc_var_new(&new_host);

    when_null_trace(interfaces, exit, ERROR, "Something bad happened");

    interface_var = GET_ARG(interfaces, interface);
    if(interface_var == NULL) {
        amxc_var_add_key(bool, args, "interface_down", true);
        set_interface(function_name, args, ret); //Add interface
        interface_var = GET_ARG(interfaces, interface);
    }
    when_null_trace(interface_var, exit, ERROR, "Interface '%s' not known, can't add host '%s'", interface, hostname);
    hosts = GET_ARG(interface_var, "hosts");
    when_null_trace(hosts, exit, ERROR, "Something bad happened");

    host = GET_ARG(hosts, hostname);
    if(host == NULL) {
        SAH_TRACEZ_INFO(ME, "Add new host '%s'", hostname);
        amxc_var_set_type(new_host, AMXC_VAR_ID_HTABLE);
        for(int i = 0; list[i] != NULL; i++) {
            amxc_var_add_key(amxc_llist_t, new_host, list[i], NULL);
        }
    } else {
        amxc_var_copy(new_host, host);
    }

    for(int i = 0; list[i] != NULL; i++) {
        amxc_var_t* ip_list = GET_ARG(new_host, list[i]);
        amxc_var_for_each(var, GET_ARG(args, list[i])) {
            const char* ip_address = GET_CHAR(var, NULL);
            SAH_TRACEZ_INFO(ME, "Add new address '%s' to %s", ip_address, list[i]);
            amxc_var_add(cstring_t, ip_list, ip_address);
            count++;
        }
    }

    when_false_trace(count > 0, exit, ERROR, "Nothing added");

    if(g.state == RUNNING) {
        if(!no_failover) {
            check_critical = true;
        }
        status = cmd_add_host(interface, hostname, GET_CHAR(interface_var, "domain_name"),
                              GET_ARG(args, "ipv4_addresses"), GET_ARG(args, "ipv6_addresses"), false);
        when_failed(status, exit);
        apply_host_cfg(hosts, hostname, host, new_host);
    } else {
        apply_host_cfg(hosts, hostname, host, new_host);
        if(g.enable) {
            handle_unbound(__func__);
        }
    }

    rv = 0;
exit:
    if(check_critical && (status != unbound_ok) && (status < unbound_other_error)) {
        SAH_TRACEZ_ERROR(ME, "Critical Error: Unbound will restart.");
        apply_host_cfg(hosts, hostname, host, new_host);
        handle_unbound(__func__);
    }
    amxc_var_delete(&new_host);
    return rv;
}

static int remove_host(UNUSED const char* function_name,
                       amxc_var_t* args,
                       UNUSED amxc_var_t* ret) {
    unbound_status_t status = unbound_other_error;
    amxc_var_t* interfaces = GET_ARG(&g.config, "interfaces");
    amxc_var_t* interface_var = NULL;
    amxc_var_t* host = NULL;
    amxc_var_t* hosts = NULL;
    const char* interface = GET_CHAR(args, "interface");
    const char* hostname = GET_CHAR(args, "name");
    bool no_failover = GET_BOOL(args, "no_failover");
    int rv = -1;
    bool check_critical = false;

    when_null_trace(interfaces, exit, ERROR, "Something bad happened");
    interface_var = GET_ARG(interfaces, interface);
    when_null_trace(interface_var, exit, ERROR, "Interface '%s' not known, can't remove host '%s'", interface, hostname);
    hosts = GET_ARG(interface_var, "hosts");
    when_null_trace(hosts, exit, ERROR, "Something bad happened");
    host = GET_ARG(hosts, hostname);
    when_null_trace(host, exit, ERROR, "Interface '%s' has no host '%s'", interface, hostname);

    SAH_TRACEZ_INFO(ME, "Remove host '%s'", hostname);
    if(g.state == RUNNING) {
        if(!no_failover) {
            check_critical = true;
        }
        status = cmd_remove_host(interface, hostname, GET_CHAR(interface_var, "domain_name"));
        when_failed(status, exit);
        apply_host_cfg(hosts, hostname, host, NULL);
    } else {
        apply_host_cfg(hosts, hostname, host, NULL);
        if(g.enable) {
            handle_unbound(__func__);
        }
    }

    if(!GET_BOOL(interface_var, "up") && (amxc_var_get_first(hosts) == NULL)) {
        remove_interface(function_name, args, ret);
    }

    rv = 0;
exit:
    if(check_critical && (status != unbound_ok) && (status < unbound_other_error)) {
        SAH_TRACEZ_ERROR(ME, "Critical Error: Unbound will restart.");
        apply_host_cfg(hosts, hostname, host, NULL);
        if(!GET_BOOL(interface_var, "up") && (amxc_var_get_first(hosts) == NULL)) {
            remove_interface(function_name, args, ret);
        }
        handle_unbound(__func__);
    }
    return rv;
}

static int remove_host_address(UNUSED const char* function_name,
                               amxc_var_t* args,
                               UNUSED amxc_var_t* ret) {
    unbound_status_t status = unbound_other_error;
    amxc_var_t* interfaces = GET_ARG(&g.config, "interfaces");
    amxc_var_t* interface_var = NULL;
    amxc_var_t* host = NULL;
    amxc_var_t* new_host = NULL;
    amxc_var_t* hosts = NULL;
    const char* interface = GET_CHAR(args, "interface");
    const char* hostname = GET_CHAR(args, "name");
    int count = 0;
    int rv = -1;
    bool check_critical = false;

    amxc_var_new(&new_host);

    when_null_trace(interfaces, exit, ERROR, "Something bad happened");
    interface_var = GET_ARG(interfaces, interface);
    when_null_trace(interface_var, exit, ERROR, "Interface '%s' not known, can't remove host '%s'", interface, hostname);
    hosts = GET_ARG(interface_var, "hosts");
    when_null_trace(hosts, exit, ERROR, "Something bad happened");
    host = GET_ARG(hosts, hostname);
    when_null_trace(host, exit, ERROR, "Interface '%s' has no host '%s'", interface, hostname);
    amxc_var_copy(new_host, host);

    for(int i = 0; list[i] != NULL; i++) {
        amxc_var_for_each(var_a, GET_ARG(args, list[i])) {
            const char* ip_address = GET_CHAR(var_a, NULL);

            amxc_var_for_each(var_b, GET_ARG(new_host, list[i])) {
                if(strcmp(GET_CHAR(var_b, NULL), ip_address) == 0) {
                    SAH_TRACEZ_INFO(ME, "Remove ip address '%s' from %s", ip_address, list[i]);
                    amxc_var_delete(&var_b);
                    count++;
                    break;
                }
            }
        }
    }

    when_false_trace(count > 0, exit, ERROR, "Nothing removed");

    if(g.state == RUNNING) {
        // we could patch unbound to make it possible to remove addresses in 1 call
        check_critical = true;
        status = cmd_remove_host(interface, hostname, GET_CHAR(interface_var, "domain_name"));
        when_failed(status, exit);
        status = cmd_add_host(interface, hostname, GET_CHAR(interface_var, "domain_name"),
                              GET_ARG(new_host, "ipv4_addresses"), GET_ARG(new_host, "ipv6_addresses"), true);
        when_failed(status, exit);
        apply_host_cfg(hosts, hostname, host, new_host);
    } else {
        apply_host_cfg(hosts, hostname, host, new_host);
        if(g.enable) {
            handle_unbound(__func__);
        }
    }

    if(!GET_BOOL(interface_var, "up") && (amxc_var_get_first(hosts) == NULL)) {
        remove_interface(function_name, args, ret);
    }

    rv = 0;
exit:
    if(check_critical && (status != unbound_ok) && (status < unbound_other_error)) {
        SAH_TRACEZ_ERROR(ME, "Critical Error: Unbound will restart.");
        apply_host_cfg(hosts, hostname, host, new_host);
        if(!GET_BOOL(interface_var, "up") && (amxc_var_get_first(hosts) == NULL)) {
            remove_interface(function_name, args, ret);
        }
        handle_unbound(__func__);
    }
    amxc_var_delete(&new_host);
    return rv;
}

static int set_options(UNUSED const char* function_name,
                       amxc_var_t* args,
                       UNUSED amxc_var_t* ret) {
    amxc_var_t* options = GET_ARG(&g.config, "options");
    amxc_var_copy(options, args);
    return 0;
}

static int set_cache(UNUSED const char* function_name,
                     amxc_var_t* args,
                     UNUSED amxc_var_t* ret) {
    amxc_var_t* cache = GET_ARG(&g.config, "cache");

    if(cache == NULL) {
        cache = amxc_var_add_new_key(&g.config, "cache");
        when_null(cache, exit);
    }

    amxc_var_move(cache, args);

    // Unbound 1.17.1 needs to restart for cache size
    create_config_file();
    if(g.enable) {
        handle_unbound(__func__);
    }

exit:
    return 0;
}

static int flush_cache(UNUSED const char* function_name,
                       UNUSED amxc_var_t* args,
                       UNUSED amxc_var_t* ret) {
    return cmd_flush_cache();
}

static void apply_domain_cfg(amxc_var_t* intf_var, const char* domain_name) {
    amxc_var_t* domain_name_var = NULL;

    when_null(intf_var, exit);

    domain_name_var = GET_ARG(intf_var, "domain_name");
    if(domain_name_var == NULL) {
        amxc_var_add_key(cstring_t, intf_var, "domain_name", domain_name);
    } else {
        amxc_var_set(cstring_t, domain_name_var, domain_name);
    }
    create_config_file();

exit:
    return;
}

static int set_domain_name(UNUSED const char* function_name,
                           amxc_var_t* args,
                           UNUSED amxc_var_t* ret) {
    amxc_var_t* interfaces = GET_ARG(&g.config, "interfaces");
    amxc_var_t* intf_var = NULL;
    amxc_var_t* hosts = NULL;
    amxc_var_t* args_remove = NULL;
    amxc_var_t* args_add = NULL;
    int rv = -1;
    const char* interface = GET_CHAR(args, "interface");
    const char* domain_name = GET_CHAR(args, "domain_name");

    amxc_var_new(&args_remove);
    amxc_var_new(&args_add);

    // Empty domain name is allowed, but NULL not
    when_null_trace(domain_name, exit, ERROR, "Domain Name cannot be NULL");

    when_null_trace(interfaces, exit, ERROR, "Something bad happened");
    intf_var = GET_ARG(interfaces, interface);
    when_null_trace(intf_var, exit, ERROR, "No config found for interface %s", interface);

    if(g.state != RUNNING) {
        apply_domain_cfg(intf_var, domain_name);
        if(g.enable) {
            handle_unbound(__func__);
        }
        rv = 0;
        goto exit;
    }

    amxc_var_set_type(args_remove, AMXC_VAR_ID_LIST);
    amxc_var_set_type(args_add, AMXC_VAR_ID_LIST);

    hosts = GET_ARG(intf_var, "hosts");
    amxc_var_for_each(host, hosts) {
        amxc_var_t* args_remove_host_address = NULL;
        amxc_var_t* args_add_host = NULL;
        const char* hostname = amxc_var_key(host);
        if(string_empty(hostname)) {
            continue;
        }
        if(!is_local_hostname_base(hostname)) {
            continue;
        }
        args_remove_host_address = amxc_var_add(amxc_htable_t, args_remove, NULL);
        args_add_host = amxc_var_add(amxc_htable_t, args_add, NULL);
        amxc_var_copy(args_remove_host_address, args);
        amxc_var_copy(args_add_host, args);
        amxc_var_add_key(cstring_t, args_remove_host_address, "name", hostname);
        amxc_var_add_key(cstring_t, args_add_host, "name", hostname);
        amxc_var_add_key(bool, args_remove_host_address, "no_failover", true);
        amxc_var_add_key(bool, args_add_host, "no_failover", true);
        for(int i = 0; list[i] != NULL; i++) {
            amxc_var_t* ip_list = GET_ARG(args_add_host, list[i]);
            if(ip_list == NULL) {
                ip_list = amxc_var_add_key(amxc_llist_t, args_add_host, list[i], NULL);
            }
            amxc_var_for_each(address, GET_ARG(host, list[i])) {
                const char* ip_address = GET_CHAR(address, NULL);
                amxc_var_add(cstring_t, ip_list, ip_address);
            }
        }
    }

    amxc_var_for_each(function_args, args_remove) {
        rv = remove_host(function_name, function_args, ret);
        if(rv != 0) {
            apply_domain_cfg(intf_var, domain_name);
            if(g.enable) {
                handle_unbound(__func__);
            }
            rv = 0;
            goto exit;
        }
    }

    apply_domain_cfg(intf_var, domain_name);

    amxc_var_for_each(function_args, args_add) {
        rv = add_host(function_name, function_args, ret);
        if(rv != 0) {
            if(g.enable) {
                handle_unbound(__func__);
            }
            rv = 0;
            goto exit;
        }
    }

    rv = 0;
exit:
    amxc_var_delete(&args_remove);
    amxc_var_delete(&args_add);
    return rv;
}

static void wait_for_child(UNUSED amxp_timer_t* local_timer, UNUSED void* priv) {
    SAH_TRACEZ_ERROR(ME, "Bug: failed to receive " WAIT_DONE_SIGNAL " after %dsec", WAIT_FOR_CHILD_IN_SEC);
}

typedef struct {
    const char* name;
    amxm_callback_t callback;
} function_t;

static AMXM_CONSTRUCTOR module_init(void) {
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;
    function_t function[] = {
        {"set-server", set_server},
        {"add-server", add_server},
        {"remove-server", remove_server},
        {"add-host", add_host},
        {"remove-host", remove_host},
        {"remove-host-address", remove_host_address},
        {"set-interface", set_interface},
        {"remove-interface", remove_interface},
        {"enable-changed", enable_changed},
        {"set-options", set_options},
        {"set-cache", set_cache},
        {"flush-cache", flush_cache},
        {"rebind-changed", rebind_protection_changed},
        {"set-domain-name", set_domain_name},
        {NULL, 0}
    };
    int rv = -1;
    amxc_var_t* var = NULL;

    memset(&g, 0, sizeof(globals_t));

    rv = amxm_module_register(&mod, so, "dns");
    when_false(0 == rv, exit);

    for(size_t i = 0; function[i].name != NULL; i++) {
        if(0 != amxm_module_add_function(mod, function[i].name, function[i].callback)) {
            SAH_TRACEZ_WARNING(ME, "Failed to register function %s", function[i].name);
        }
    }

    rv = amxp_subproc_new(&g.process);
    when_false(rv == 0, exit);
    amxp_slot_connect(g.process->sigmngr, "stop", NULL, unbound_stopped, NULL);

    amxp_timer_new(&g.start_timer, delayed_start, NULL);
    amxp_timer_new(&g.wait_for_child, wait_for_child, NULL);

    g.enable = true;
    update_state(STOPPED, __func__);
    g.timeout = -1;
    g.repeat = -1;
    g.start_timer_in_ms = DELAY_TIME;
    g.rebind.enable = true;
    amxc_var_init(&g.rebind.domains);
    amxc_var_set_type(&g.rebind.domains, AMXC_VAR_ID_LIST);
    amxc_var_init(&g.rebind.ips);
    amxc_var_set_type(&g.rebind.ips, AMXC_VAR_ID_LIST);

    amxc_var_init(&g.config);
    amxc_var_set_type(&g.config, AMXC_VAR_ID_HTABLE);
    var = amxc_var_add_key(amxc_htable_t, &g.config, "fwzones", NULL);
    amxc_var_add_key(amxc_htable_t, var, ".", NULL);
    amxc_var_add_key(amxc_htable_t, &g.config, "interfaces", NULL);
    amxc_var_add_key(amxc_llist_t, &g.config, "options", NULL);

    amxp_dir_make("/var/lib", 0755);
    amxp_slot_connect(NULL, WAIT_DONE_SIGNAL, NULL, unbound_wait_done, NULL);

exit:
    if(rv != 0) {
        amxp_subproc_delete(&g.process);
    }
    return rv;
}

static AMXM_DESTRUCTOR module_exit(void) {
    stop_unbound(STOPPED);

    amxp_subproc_delete(&g.process);
    amxp_timer_delete(&g.start_timer);
    amxp_timer_delete(&g.wait_for_child);
    amxc_var_clean(&g.rebind.ips);
    amxc_var_clean(&g.rebind.domains);
    amxc_var_clean(&g.config);
    amxp_slot_disconnect(NULL, WAIT_DONE_SIGNAL, unbound_wait_done);

    return 0;
}
