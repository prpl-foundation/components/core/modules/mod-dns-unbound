# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.6.13 - 2024-12-03(14:40:09 +0000)

### Other

- - [TR181-DNS] Unbound should not be restarted when changing the Domain Name

## Release v1.6.12 - 2024-11-25(09:28:20 +0000)

### Other

- - [tr181-dns] unbound cache size is configured very small because of bug

## Release v1.6.11 - 2024-09-05(16:27:52 +0000)

### Other

- Add dns flags to NetModel
- Unbound start/stop burst

## Release v1.6.10 - 2024-06-25(13:20:08 +0000)

### Other

- host for loopback interface

## Release v1.6.9 - 2024-06-17(15:34:23 +0000)

### Other

- DomainName should be supported and should also be used for DNS suffix

## Release v1.6.8 - 2024-06-06(11:13:56 +0000)

### Other

- Unbound is restarted for every listen interface address change

## Release v1.6.7 - 2024-05-28(11:29:40 +0000)

### Other

- - set_server changes ignored

## Release v1.6.6 - 2024-05-27(10:37:20 +0000)

### Fixes

- 2 Cdrouter tests are failing for all test jobs

## Release v1.6.5 - 2024-04-11(09:11:11 +0000)

### Fixes

- Device.DNS.X_PRPL-COM_RebindProtection.IPExceptions.{i}.Address not accepting IP from 127.0.0.x/x

## Release v1.6.4 - 2024-02-16(13:55:41 +0000)

### Fixes

- DUT doesn't resolve its hostname

## Release v1.6.3 - 2024-01-09(08:46:51 +0000)

### Fixes

-  [Ethernet_PPP6] wan KO after reboot

## Release v1.6.2 - 2024-01-04(08:51:01 +0000)

### Fixes

- unbound immediately returns servfail because of timeout 0

## Release v1.6.1 - 2023-11-30(08:54:12 +0000)

### Fixes

- [prplOS] tr181-dns: DNS service is not available without WAN uplink

## Release v1.6.0 - 2023-11-27(12:07:15 +0000)

### New

- generic DNS failover mechanism

## Release v1.5.0 - 2023-11-20(11:01:42 +0000)

### New

- prplOS DNS rebinding

## Release v1.4.3 - 2023-11-07(14:20:54 +0000)

### Fixes

- reduce compile time by removing old dependencies

## Release v1.4.2 - 2023-10-28(07:13:24 +0000)

### Fixes

- Issue HOP-2107 CLONE - [PRPL][DNS][Regression] lan DHCP client hostname is not resolved

## Release v1.4.1 - 2023-10-04(07:54:23 +0000)

### Other

- Change unbound dependency to unbound-daemon-prpl

## Release v1.4.0 - 2023-08-07(11:15:51 +0000)

### New

- [amx][DNS] Add support for Forward Zones in the DNS plugin (and tr181 dm)

## Release v1.3.0 - 2023-07-13(12:29:15 +0000)

### New

- dns failover after 800ms

## Release v1.2.0 - 2023-06-29(13:06:10 +0000)

### New

- use amx to communicate with Unbound

## Release v1.1.3 - 2023-06-26(14:00:59 +0000)

### Other

- Add missing native dependency on protobuf-c

## Release v1.1.2 - 2023-06-19(14:53:42 +0000)

### Other

- Create /var/lib at runtime
- Opensource component

## Release v1.1.1 - 2023-06-17(06:39:23 +0000)

### Fixes

- Add empty directory /var/lib needed by unbound

## Release v1.1.0 - 2023-06-08(17:14:15 +0000)

### New

- Implement DNS caching

## Release v1.0.6 - 2023-03-20(09:48:39 +0000)

### Fixes

- The DNS proxy seems not working on the Box LAN GUA IPv6@

## Release v1.0.5 - 2023-02-27(12:43:31 +0000)

### Fixes

- [CDROUTER][DNS] The DNS proxy seems not working on the Box LAN GUA IPv6@

## Release v1.0.4 - 2023-02-23(09:46:02 +0000)

### Fixes

- read handler loop when recv returns 0

## Release v1.0.3 - 2023-01-13(08:24:02 +0000)

### Fixes

- sometimes unbound won't restart

## Release v1.0.2 - 2023-01-12(11:15:04 +0000)

### Fixes

- when wan interface goes down there are many logs "recv error: 0"

## Release v1.0.1 - 2023-01-09(13:49:43 +0000)

### Fixes

- DNS KO and many syslog traces "recv failed: Invalid argument"

## Release v1.0.0 - 2023-01-06(14:47:44 +0000)

### Breaking

- Hosts, HostName and DomainName configuration

## Release v0.2.2 - 2022-08-23(08:56:12 +0000)

### Fixes

- The log gets spammed with Incomplete message (have 1024 bytes, want 65593)

## Release v0.2.1 - 2022-08-19(08:41:03 +0000)

### Other

- Add debug traces

## Release v0.2.0 - 2022-04-28(11:45:37 +0000)

### New

- must support failover scenarios

## Release v0.1.1 - 2022-04-12(07:07:24 +0000)

### Fixes

- mod_dns-unbound depends on Unbound

## Release v0.1.0 - 2022-04-07(05:29:22 +0000)

### New

- It must be possible to support 'natively' a caching dnsserver

